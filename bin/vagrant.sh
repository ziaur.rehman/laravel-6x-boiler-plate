#!/bin/bash

PHP_VERSION=7.3 #PHP version e.g 7.2, or 7.3

cd $HOME
function installPHP() {
    php="php${PHP_VERSION}"
    
    sudo add-apt-repository ppa:ondrej/php
    sudo apt-get update
    sudo apt-get install -yq "${php}" \
        php-memcached \
        "${php}-dev" \
        "libapache2-mod-${php}" \
        "${php}-gd" \
        "${php}-bcmath" \
        "${php}-curl" \
        "${php}-json" \
        "${php}-xml" \
        "${php}-mbstring" \
        "${php}-mysql" \
        "${php}-zip"
}

installPHP
